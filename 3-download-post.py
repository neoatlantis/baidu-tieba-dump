#!/usr/bin/env python3

import sys
import os
import re

from bs4 import BeautifulSoup as BS
import requests


BASEPATH = os.path.realpath(os.path.dirname(sys.argv[0]))

try:
    taskfile = os.path.realpath(os.path.join(BASEPATH, sys.argv[1]))
    outputdir = os.path.dirname(taskfile)
    print("Task file: %s" % taskfile)
    print("Download to: %s" % outputdir)
    assert os.path.isdir(outputdir)
    assert os.path.isfile(taskfile)
except:
    print("Usage: python3 3-download-post.py <taskfile>")
    exit(1)




SAVENAME = lambda tid, cur, count: \
    os.path.join(outputdir, "post-%d-%dof%d.html" % (tid, cur, count))
DOWNLOAD_URL = lambda tid, cur=1: \
    "https://tieba.baidu.com/mo/q---1193C9D82978565A5991B605073E8834%3AFG%3D1--1-3-0--2--wapp_1504519399499_146/m?" + \
    "pnum=%d" % cur + \
    "&kz=%s" % tid


# build index for existing tasks

EXISTING = {}
filenameMatcher = re.compile(r"post\-([0-9]+)\-([0-9]+)of([0-9]+)\.html")

for each in os.listdir(outputdir):
    m = filenameMatcher.match(each)
    if not m: continue
    tid = int(m.group(1))
    cur = int(m.group(2))
    count = int(m.group(3))

    if tid not in EXISTING:
        EXISTING[tid] = {}
    EXISTING[tid][cur] = count

##############################################################################

def downloadPage(tid, cur=1):
    url = DOWNLOAD_URL(tid, cur)
    r = requests.get(url)
    return r.content

def findTotal(src):
    soup = BS(src)
    try:
        forms = soup.findAll('form', {"action": "m"})
        form = forms[0]
        html = form.get_text()
        p = re.findall("\/([0-9]+)页", html)
        return int(p[0])
    except:
        return 1

def downloadPost(tid):
    global EXISTING

    total = None
    cur = 1

    if tid in EXISTING:
        # open a existing file to see total pages
        try:
            pages = list(EXISTING[tid].keys())
            if len(pages) > 0:
                probePage = pages[0]
                probefn = SAVENAME(tid, probePage, EXISTING[tid][probePage])
                content = open(probefn, 'r').read()
                total = findTotal(content)
                print("Previous results show total %d pages for post %d." % (
                    total,
                    tid
                ))
        except:
            total = None # failed in finding total

    if total:
        # previous results exists
        totalFound = True
    else:
        total = 1
        totalFound = False

    while True:
        if tid in EXISTING and cur in EXISTING[tid]:
            print("Skip download for page %d in post %d." % (cur, tid))
        else:
            print("Downloading: %d/%d for %d." % (cur, total, tid))
            src = downloadPage(tid, cur)
            if not totalFound:
                try:
                    total = findTotal(src)
                    totalFound = True
                except:
                    break

            fn = SAVENAME(tid, cur, total)
            print(" >> ", fn)
            with open(fn, 'wb+') as f:
                f.write(src)

        cur += 1
        if cur > total:
            break


k = 1

with open(taskfile, 'r') as tf:
    while True:
        print("-" * 30 + " %d " % k + "-" * 30)
        l = tf.readline()
        if not l: break
        l = l.strip()

        try:
            tid = int(l)
        except KeyboardInterrupt:
            break
        except:
            continue

        try:
            downloadPost(tid)
        except KeyboardInterrupt:
            break
        except:
            continue
        k += 1
