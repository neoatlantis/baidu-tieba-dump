#!/usr/bin/env python3

import sys

try:
    tbname, total = sys.argv[1:3]
    total = int(total)
except:
    print("Usage: python3 1-taskgen-tieba-indexes.py <TIEBA NAME> <PAGE COUNT>")
    exit(1)



def urlgen(tbname, i, total):
    assert type(tbname) == str
    tbnameEncoded = ''.join(['%%%x' % i for i in tbname.encode('utf-8')]).upper()

    return \
        "https://tieba.baidu.com/mo/q---1193C9D82978565A5991B605073E8834%3AFG%3D1--1-3-0--2--wapp_1504519399499_146/m?pnum=" + \
        ("%d" % i) + \
        "&lm=&tnum=%d&" % (10 * total) + \
        "kw=%s" % tbnameEncoded + \
        "&lp=5009&pinf=1_2_0&sub=%E8%B7%B3%E9%A1%B5"

for i in range(0, total):
    p = i+1
    print(urlgen(tbname, p, total))
