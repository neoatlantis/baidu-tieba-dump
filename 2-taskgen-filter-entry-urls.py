#!/usr/bin/env python3

"""Filter all URLs for posts from downloaded index files."""

import sys
import os
import re

from bs4 import BeautifulSoup as BS


BASEPATH = os.path.realpath(os.path.dirname(sys.argv[0]))

ifilepath = sys.argv[1]
if not ifilepath:
    print("Usage: python3 2-taskgen-filter-entry-urls.py <inputdir> ... > task2.txt")
    exit(1)

ifilepath = os.path.join(BASEPATH, ifilepath)
assert os.path.isdir(ifilepath)

ifiles = [os.path.join(ifilepath, e) for e in os.listdir(ifilepath)]
total = len(ifiles)
sys.stderr.write("%d files found.\n" % total)

finder = re.compile(r"kz=([0-9]+)")

c = 0
for ifile in ifiles:
    c += 1
    try:
        src = BS(open(ifile, 'r').read())
    except: 
        continue

    for a in src.findAll('a'):
        href = a.get('href')
        if not href: continue
        if "kz=" in href:
            x = finder.findall(href)
            print(x[0])

    sys.stderr.write("%d / %d done.\n" % (c, total))
